from jira.client import JIRA
import pygal

def JIRA_Connection():
    jira_options={'server': 'http://localhost:8080'}

    jira=JIRA(options=jira_options,basic_auth=("klus","klus"))

    issues_in_project = jira.search_issues('project = EB')

    bar_chart = pygal.Bar()

    RedIssuelist = 0
    GreenIssuelist = 0

    for issue in issues_in_project:
       if str(issue.fields.customfield_10107) == "Red":
            print (str(issue) + ":" + 'Red')
            RedIssuelist += 1


       elif str(issue.fields.customfield_10107) == "Green":
            print ( str(issue) + ':' + str(issue.fields.customfield_10107))
            GreenIssuelist += 1


       else:
            print(str(issue) + ':' + str(issue.fields.customfield_10107))

    bar_chart.add('Red', RedIssuelist)
    bar_chart.add('Green', GreenIssuelist)
    bar_chart.render_to_file('jira_bar_chart.svg')
def main():
    JIRA_Connection()

if __name__ == "__main__":
    main()




